/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function printUserInfo(){
		let name = prompt("What is your full name?");
		let age = prompt("How old are you?");
		let address = prompt("Where do you live?");
		alert("Thank you for your input!");
		console.log("Hello, " + name);
		console.log("You are " + age + " years old.");
		console.log("You live in " + address);
	}

	printUserInfo();

/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function printMyTopFiveBands(){
		console.log("My Top Five Favorite Bands")
		console.log("1. Parokya ni Edgar");
		console.log("2. RedJumpSuit Apparatus");
		console.log("3. Linkin Park");
		console.log("4. Hoobastank");
		console.log("5. My Chemical Romance");
	}

	printMyTopFiveBands();

/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function printMyTopFiveMovies(){
		console.log("My Top Five Favorite Movies")
		console.log("1. Halloween Ends")
		console.log("Rotten Tomatoes Rating: 41%")
		console.log("2. The School for Good and Evil")
		console.log("Rotten Tomatoes Rating: 36%")
		console.log("3. The Stranger")
		console.log("Rotten Tomatoes Rating: 94%")
		console.log("4. Werewolf by Night")
		console.log("Rotten Tomatoes Rating: 91%")
		console.log("5. X")
		console.log("Rotten Tomatoes Rating: 94%")
	}

	printMyTopFiveMovies();


/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

